##############################################################################
# Project: Kish - Major Joint Replacement 
# Date: 5/19/14
# Author: Sam Bussmann
# Description: Demographic Output
# Notes: 
##############################################################################

drg<-as.data.frame(as.matrix(coef(cvnet1,s = "lambda.min")))
drg2<-data.frame(Varname=row.names(drg),Value=drg[,"1"])
write.csv(drg2,file="drg_coeff.csv")

icd9<-as.data.frame(as.matrix(coef(cvnet.icd9,s = "lambda.min")))
icd92<-data.frame(Varname=row.names(icd9),Value=icd9[,"1"])
write.csv(icd92,file="icd9_coeff.csv")


varname="self_employed"

qq<-unique(quantile(pm[,varname], 
                    probs=seq.int(0,1, length.out=11)))
out <- cut(pm[,varname], breaks=10, include.lowest=TRUE)

a1<-by(pm$pt_drg,list(out),mean)
a2<-by(pm$pt_drg,list(out), function(x) {sum(!is.na(x))})

par(mfrow=c(2,1))
barplot(as.numeric(a2),names.arg=names(a2),col="lightblue")
barplot(as.numeric(a1),names.arg=names(a1),col="yellow")


out<-binmin2(varname1="HouseholdMemberCount", minN=150000, df=pm)
viz4("HouseholdMemberCount",out)

### Income plot of top deciles



intop_icd9 <- (pm$CommunityPersonID %in% tog_icd9_3$CommunityPersonID[out1 %in% c("1","2")] )
intop_drg <- (pm$CommunityPersonID %in% tog_drg_3$CommunityPersonID[out %in% c("1","2")] )

par(mfrow=c(2,1))

hist(pm$FindIncome[intop_icd9],xlim=c(0,325000),breaks=25,xlab="Income",main="Income of Top Two Deciles for DRG Target",
     col="lightblue")
hist(pm$FindIncome[intop_drg],xlim=c(0,325000),breaks=25,xlab="Income",main="Income of Top Two Deciles for ICD9 Target",
     col="lightblue")

hist(as.numeric(pm$ExactAge[intop_icd9 & !is.na(pm$ExactAge)]),xlim=c(25,85),breaks=15,
     xlab="Age",main="Age of Top Two Deciles for ICD9 Target",col="lightgreen")
hist(as.numeric(pm$ExactAge[intop_drg & !is.na(pm$ExactAge)]),xlim=c(25,85),breaks=15,
     xlab="Age",main="Age of Top Two Deciles for DRG Target",col="lightgreen")
